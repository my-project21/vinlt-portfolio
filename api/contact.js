import axios from "api";

export const sendEmail = async (form) => {
  try {
    const { data } = await axios.post("/api/contact", form);

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};
