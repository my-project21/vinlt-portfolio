// Libraries
import React from "react";
import Image from "next/image";
import PropTypes from "prop-types";

const defaultMenu = [
  { value: "home", label: "Home" },
  { value: "about", label: "About" },
  { value: "skills", label: "Skills" },
  { value: "experiences", label: "Experiences" },
  { value: "portfolio", label: "Portfolio" },
  { value: "contact", label: "Contact" },
];

const Header = () => {
  return (
    <div className="xl:absolute sticky top-0 w-full bg-white z-50 animate__animated animate__fadeInDown">
      <div className="container mx-auto py-5 flex items-center justify-between">
        <a href="#home">
          <Image
            src="/images/nltruongvi-logo.png"
            width={140}
            height={41}
            alt="Vinlt portfolio"
          />
        </a>
        <div
          id="menu"
          className="lg:flex hidden text-lg font-medium items-center gap-5"
        >
          {defaultMenu.length
            ? defaultMenu.map((item) => {
                return (
                  <div
                    data-menuanchor={item.value}
                    className="menu-item cursor-pointer border-b border-transparent text-gray-800 transition-all hover:text-cyan-700 duration-300"
                    key={item.value}
                  >
                    <a href={`#${item.value}`}>{item.label}</a>
                  </div>
                );
              })
            : null}
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
