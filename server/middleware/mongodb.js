import mongoose from "mongoose";

const connectDB = (handler) => async (req, res) => {
  if (mongoose.connections[0].readyState) {
    // Use current db connection
    return handler(req, res);
  }
  // Use new db connection
  await mongoose
    .connect(
      "mongodb+srv://nltruongvi:TjmWjm824594@cluster0.vzakd.mongodb.net/vinlt-portfolio?retryWrites=true&w=majority",
      {
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useNewUrlParser: true,
      }
    )
    .then(() => {
      console.log("connect success");
    });
  return handler(req, res);
};

export default connectDB;
