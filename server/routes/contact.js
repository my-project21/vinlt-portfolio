// Libraries
const router = require("express").Router();

// Controller
const { createContact } = require("../controllers/contact");

router.post("/", createContact);

module.exports = router;
