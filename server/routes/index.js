// Libraries
const express = require("express");
const router = express.Router();

// Routes
const contactRoute = require("./contact");

module.exports = (app) => {
  router.use("/contact", contactRoute);

  app.use("/api", router);
};
