// Libraries
import React from "react";
import Lottie from "react-lottie";

// Lottie animation
import developerAnimation from "public/Lottie/developer.json";

const defaultSkills = [
  "Html",
  "Css",
  "Scss",
  "React",
  "Javascript",
  "Redux",
  "Antd design",
];

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: developerAnimation,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

function AboutSection() {
  return (
    <div className="grid lg:grid-cols-2 grid-cols-1 gap-4 text-center md:text-left">
      <div id="lottie-develop" className="animate__animated">
        <Lottie options={defaultOptions} />
      </div>
      <div className="flex justify-center flex-col">
        <h2
          id="about-me-title"
          className="animate__animated animate__slow text-3xl font-semibold"
        >
          About me
        </h2>
        <p
          id="about-me-description"
          className="my-5 animate__animated animate__slow"
        >
          Hi, My name is Vi. I&apos;m a front-end developer with 2 year
          experiences. Also I am good at
        </p>
        <div
          id="good-at"
          className="flex flex-wrap gap-4 text-sm justify-center md:justify-start animate__animated animate__slow"
        >
          {defaultSkills.length
            ? defaultSkills.map((skill) => (
                <div
                  key={skill}
                  className="py-1 px-3 border border-cyan-700 rounded-sm"
                >
                  {skill}
                </div>
              ))
            : null}
        </div>
        <p
          id="about-me-description-2"
          className="my-5 animate__animated animate__slow"
        >
          Nice to meet you and hopeful we will be partner
        </p>
        <a
          id="cv-btn"
          aria-label="Portfolio pdf"
          className="py-2 px-7 mx-auto md:mx-0 animate__animated animate__slow bg-cyan-700 hover:bg-cyan-600 transition-all rounded-full w-max text-white"
          href="/pdf/Nguyen-Luong-Truong-Vi-CV-xin-viec.pdf"
          target="_blank"
        >
          Download CV
          <i className="icon-portfolio-download ml-2"></i>
        </a>
      </div>
    </div>
  );
}

export default AboutSection;
