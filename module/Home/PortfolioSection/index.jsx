// Libraries
import React, { useState } from "react";
import Image from "next/image";

// Styles
import styles from "./styles.module.scss";

function PortfolioSection() {
  const [cols] = useState([
    {
      value: "col-1",
      projects: [
        {
          value: "moneytech",
          label: "Moneytech",
          image: "/images/projects/moneytech.png",
          height: "h-72",
        },
        {
          value: "habimecgroup",
          label: "Habimecgroup",
          image: "/images/projects/habimec.jpg",
          height: "h-52",
        },
      ],
    },
    {
      value: "col-2",
      projects: [
        {
          value: "social-chat",
          label: "Social chat",
          image: "/images/projects/social-chat.png",
          height: "h-52",
        },
        {
          value: "error-monitoring",
          label: "Error monitoring",
          image: "/images/projects/error-monitoring.jpg",
          height: "h-72",
        },
      ],
    },
    {
      value: "col-3",
      projects: [
        {
          value: "propzy",
          label: "Propzy Tet",
          image: "/images/projects/propzy.png",
          height: "h-106",
        },
      ],
    },
  ]);

  return (
    <div className="text-center mt-10 md:mt-0">
      <h2
        id="recent-portfolio-title"
        className="animate__animated text-3xl font-semibold"
      >
        Recent Portfolio
      </h2>
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-6 mt-10">
        {cols.map((col) => (
          <div key={col.value} className="flex flex-col gap-6">
            {col.projects.map((project) => (
              <div
                id={project.value}
                key={project.value}
                className={`relative 
                                        group 
                                        cursor-pointer 
                                        ${styles["project-card"]}
                                        ${project.height} 
                                        rounded-xl 
                                        animate__animated 
                                        animate__slow 
                                        overflow-hidden 
                                        shadow-custom`}
              >
                <div className="w-full flex flex-col items-center justify-center rounded-xl text-white absolute h-full transition-all duration-500 z-10 bg-cyan-700 group-hover:bg-opacity-90 group-hover:opacity-100 opacity-0 bg-opacity-0">
                  <i className="icon-portfolio-search text-2xl"></i>
                  <div className={`font-semibold text-xl ${styles["__label"]}`}>
                    {project.label}
                  </div>
                </div>
                <Image
                  layout="fill"
                  loading="eager"
                  objectFit="cover"
                  objectPosition="top"
                  src={project.image}
                  alt={project.label}
                />
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default PortfolioSection;
