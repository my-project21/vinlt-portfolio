// Libraries
import React from "react";
import Image from "next/image";

// Default technical skills
const technicalSkills = [
  { value: "html5", label: "HTML5", icon: "/images/skills/html5.svg" },
  { value: "css3", label: "CSS3", icon: "/images/skills/css3.svg" },
  { value: "sass", label: "SASS", icon: "/images/skills/sass.svg" },
  {
    value: "javascript",
    label: "JAVASCRIPT",
    icon: "/images/skills/javascript.svg",
  },
  {
    value: "typescript",
    label: "TYPESCRIPT",
    icon: "/images/skills/typescript.svg",
  },
  { value: "react", label: "REACT", icon: "/images/skills/reactjs.svg" },
  { value: "redux", label: "REDUX", icon: "/images/skills/redux.png" },
  {
    value: "nextJs",
    label: "NEXTJS",
    icon: "/images/skills/nextjs.jpeg",
  },
  { value: "nodejs", label: "NODEJS", icon: "/images/skills/nodejs.svg" },
];

// Default soft skills
const softSkills = [
  { value: "teamwork", label: "Teamwork", icon: "icon-portfolio-teamwork" },
  {
    value: "communication",
    label: "Communication",
    icon: "icon-portfolio-conversation",
  },
  {
    value: "creativity",
    label: "Creativity",
    icon: "icon-portfolio-creativity",
  },
];

function SkillsSection() {
  return (
    <div className="grid lg:grid-cols-2 grid-cols-1 gap-10 md:gap-5 my-10 md:my-0">
      <div className="text-center">
        <h2 className="animate__animated animate__slow text-3xl font-semibold">
          Technical Skills
        </h2>
        <div className="grid grid-cols-3 gap-7 mt-10">
          {technicalSkills.length
            ? technicalSkills.map((skill) => (
                <div
                  className="flex flex-col skill-item animate__animated animate__slow items-center justify-center"
                  key={skill.value}
                >
                  <div className="w-14 h-14 relative rounded-full overflow-hidden shadow-lg">
                    <Image
                      objectFit="contain"
                      src={skill.icon}
                      alt={skill.label}
                      layout="fill"
                    ></Image>
                  </div>
                  <span className="font-medium mt-2">{skill.label}</span>
                </div>
              ))
            : null}
        </div>
      </div>
      <div className="text-center">
        <h2 className="animate__animated animate__slow text-3xl font-semibold">
          Soft Skills
        </h2>
        <div className="flex flex-col justify-center gap-7 mt-10">
          {softSkills.length
            ? softSkills.map((skill) => (
                <div
                  key={skill.value}
                  className="skill-item animate__animated animate__slow flex flex-col items-center justify-center gap-2"
                >
                  <i
                    className={`${skill.icon} text-transparent 
                                                            bg-clip-text 
                                                            bg-gradient-to-b 
                                                            from-cyan-400
                                                            to-cyan-600 text-5xl`}
                  ></i>
                  <span className="font-medium mt-2 uppercase">
                    {skill.label}
                  </span>
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}

export default SkillsSection;
