// Libraries
import React from "react";

// Default work experiences
const workExperiences = [
  {
    id: "ants-company",
    time: "Oct 2019 - Feb 2021",
    companyName: "ANTS Company",
    position: "Front-end Developer",
    responsibility: (
      <ul className="list-inside list-disc">
        <li>
          Cut Html, Css from AdobeXd, Figma, cc 2020. Executed by using reactjs
        </li>
        <li>
          Create UI Package components for ReactJs such as Ant Desgin for all
          project of company can reuse.
        </li>
        <li>
          Create Document website to guide member of dev how to use each
          component.
        </li>
      </ul>
    ),
  },
  {
    id: "sutrix-solutions",
    time: "Oct 2021 - Present",
    companyName: "Sutrix Solutions",
    position: "Front-end Developer",
    responsibility: (
      <ul className="list-inside list-disc">
        <li>
          Use tool build webs like Webflow, Wix to build Landing Page Website.
        </li>
        <li>Create web app using ReactJs</li>
        <li>Maintain and scale Client&apos;s website</li>
      </ul>
    ),
  },
];

function ExperiencesSection() {
  return (
    <div className="grid lg:grid-cols-2 grid-cols-1 gap-10">
      <div>
        <h2
          id="education"
          className="text-3xl font-semibold text-center lg:text-left"
        >
          Education
        </h2>
        <div className="flex flex-col gap-5 mt-5 md:mt-10">
          <div className="experience-card animate__animated animate__slow p-5 shadow-custom rounded-md">
            <div className="text-cyan-700 font-semibold">
              Oct 2017 - Jun 2021
            </div>
            <strong className="text-cyan-700 italic">
              VNUHCM-University of Information Technology
            </strong>
            <div className="font-semibold">Major: Software engineer</div>
            <div className="text-sm">GPA: 7.2</div>
            <p></p>
          </div>
        </div>
      </div>
      <div>
        <h2
          id="work-experience"
          className="animate__animated animate__slow text-3xl font-semibold text-center lg:text-left"
        >
          Work Experience
        </h2>
        <div className="flex flex-col gap-5 mt-5 md:mt-10">
          {workExperiences.map(
            ({ id, time, companyName, position, responsibility }) => (
              <div
                key={id}
                className="experience-card animate__animated animate__slow p-5 shadow-custom rounded-md"
              >
                <div className="text-cyan-700 font-semibold">{time}</div>
                <strong className="text-cyan-700 italic">{companyName}</strong>
                <div className="font-semibold">{position}</div>
                <div className="text-sm mt-1">
                  Responsibility :{responsibility}
                </div>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
}

export default ExperiencesSection;
