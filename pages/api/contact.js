import connectDB from "../../server/middleware/mongodb";
import { createContact } from "server/controllers/contact";

const handler = async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      createContact(req, res);
      break;

    default:
      res.json("hello world");
      break;
  }
};

export default connectDB(handler);
