// Libraries
import ReactFullPage from "@fullpage/react-fullpage";
import dynamic from "next/dynamic";

// Utils
import { addClassBySelector, isMobile } from "utils";

// Components
const Layout = dynamic(() => import("components/Layout"));
const AudioPlayer = dynamic(() => import("components/AudioPlayer"));

// Modules
const AboutSection = dynamic(() => import("module/Home/AboutSection"));
const IntroduceSection = dynamic(() => import("module/Home/IntroduceSection"));
const ExperiencesSection = dynamic(() =>
  import("module/Home/ExperiencesSection")
);
const SkillsSection = dynamic(() => import("module/Home/SkillsSection"));
const PortfolioSection = dynamic(() => import("module/Home/PortfolioSection"));
const ContactSection = dynamic(() => import("module/Home/ContactSection"));

const defaultAnchors = [
  "home",
  "about",
  "skills",
  "experiences",
  "portfolio",
  "contact",
];

export default function Home() {
  const onLeaveFullPage = (_, des) => {
    Array.from(document.querySelectorAll(".menu-item")).forEach((el) =>
      el.classList.remove("active")
    );

    document
      .querySelector(`[data-menuanchor=${des.anchor}]`)
      .classList.add("active");

    if (!isMobile.any()) {
      switch (des.index) {
        case 1:
          addClassBySelector("#lottie-develop", "animate__fadeInLeft");
          addClassBySelector("#about-me-title", "animate__fadeInUp");
          addClassBySelector("#about-me-description", "animate__fadeInUp");
          addClassBySelector("#about-me-description-2", "animate__fadeInUp");
          addClassBySelector("#good-at", "animate__fadeInUp");
          addClassBySelector("#cv-btn", "animate__fadeIn");
          break;
        case 2:
          addClassBySelector(".skill-item", "animate__fadeInUp");
          break;

        case 3:
          addClassBySelector(".experience-card", "animate__fadeInUp");
          addClassBySelector("#education", "animate__fadeInUp");
          addClassBySelector("#work-experience", "animate__fadeIn");
          break;

        case 4:
          addClassBySelector("#moneytech", "animate__fadeInDown");
          addClassBySelector("#habimecgroup", "animate__fadeInUp");
          addClassBySelector("#social-chat", "animate__fadeInDown");
          addClassBySelector("#error-monitoring", "animate__fadeInUp");
          addClassBySelector("#propzy", "animate__fadeInUp");
          addClassBySelector("#recent-portfolio-title", "animate__fadeInUp");
          break;

        case 5:
          addClassBySelector(".via-contact", "animate__fadeInUp");
          addClassBySelector("#contact-title", "animate__fadeInLeft");
          addClassBySelector("#contact-description", "animate__fadeInLeft");
          addClassBySelector(".contact-input", "animate__fadeInUp");
          addClassBySelector(".submit-btn", "animate__fadeInUp");
          break;

        default:
          break;
      }
    }
  };

  const afterRender = () => {
    document.getElementsByClassName("menu-item")[0].classList.add("active");
  };

  return (
    <Layout>
      <div className="container mx-auto text-gray-800">
        <div className="fixed top-28 left-4 z-50 opacity-0 md:opacity-100">
          <AudioPlayer />
        </div>
        <ReactFullPage
          responsiveWidth={1200}
          scrollingSpeed={1000}
          navigation
          afterRender={afterRender}
          onLeave={onLeaveFullPage}
          anchors={defaultAnchors}
          render={() => (
            <ReactFullPage.Wrapper>
              <section className="section">
                <IntroduceSection />
              </section>
              <section className="section">
                <AboutSection />
              </section>
              <section className="section">
                <SkillsSection />
              </section>
              <section className="section">
                <ExperiencesSection />
              </section>
              <section className="section">
                <PortfolioSection />
              </section>
              <section className="section">
                <ContactSection />
              </section>
            </ReactFullPage.Wrapper>
          )}
        />
      </div>
    </Layout>
  );
}
