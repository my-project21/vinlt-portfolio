require("dotenv").config();
const express = require("express"); // Sử dụng framework express
const next = require("next"); // Include module next
const mongoose = require("mongoose");

const port = parseInt(process.env.PORT, 10) || 3000; // Port để chạy app Nextjs, cũng là server nodejs
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

console.log(process.env.DB_URI);

app.prepare().then(() => {
  const server = express();

  // Middleware
  server.use(express.json());
  server.use(express.urlencoded({ extended: true }));

  // // Database connection
  // mongoose
  //   .connect(process.env.DB_URI, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true,
  //     useFindAndModify: true,
  //     useCreateIndex: true,
  //   })
  //   .then(() => {
  //     console.log("Connected to database!");
  //   })
  //   .catch((e) => {
  //     console.log(e);
  //   });

  // require("./server/routes")(server);

  // server.get("/hello/world", (req, res) => {
  //   res.json("Hello ban");
  // });

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
